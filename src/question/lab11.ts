const divEx1 = document.getElementById('lab11') as HTMLDivElement;
const textArray = document.createElement('p')
const p = document.createElement('p');
const desc = document.createElement('p');
const ans = document.createElement('div');


const button = document.createElement('input');
desc.innerText = "กรอกเลขจำนวนเต็ม 10 จำนวนเท่านั้น"
button.value = "Submit"
button.type = "submit"
textArray.innerText = "Input: "
const txtbox = document.createElement('input');
divEx1.appendChild(desc);
divEx1.appendChild(textArray);
textArray.appendChild(txtbox);
divEx1.appendChild(p);
divEx1.appendChild(button)
const divAns = document.createElement('div');
divEx1.appendChild(divAns);
divAns.appendChild(ans);



button.addEventListener('click', () => {
  divAns.innerHTML = '';
  let cleanData = txtbox.value.split(" ").join("") //ใช้ split และ join เพื่อแปลงข้อความจาก textbox เพื่อรอนำข้อมูลไปใส่ใน Array แล้วนำไปคำนวณต่อเท่านั้น

  let numberArray: number[] = [];
  if (cleanData !== '') {
    let temp: string = "";
    for (let i = 0; i < cleanData.length; i++) {
      if (cleanData[i] !== ',') {
        temp += (cleanData[i]);
        if ((i === cleanData.length - 1) && temp !== '') {
          numberArray.push(+temp)
          temp = "";
        }
        continue;
      } else {
        numberArray.push(+temp)
        temp = "";
      }
    }
  } else {
    alert("Please enter a value");
  }

  if (numberArray.length === 10) {
    for (let i = 0; i < numberArray.length - 1; i++) {
      for (let j = 0; j < numberArray.length - i - 1; j++) {
        if (numberArray[j] > numberArray[j + 1]) {
          const temp = numberArray[j];
          numberArray[j] = numberArray[j + 1];
          numberArray[j + 1] = temp;
        }
      }
    }
    ans.innerText = numberArray.toString();
    divAns.appendChild(ans);
  } else {
    alert("Please enter the number at least 10 numbers")
  }
}

)

export {

}