const divEx1 = document.getElementById('lab3') as HTMLDivElement;
const textArray = document.createElement('p')
const p = document.createElement('p');
const desc = document.createElement('p');
const ans = document.createElement('p');


const button = document.createElement('input');
desc.innerText = "กรอกเพียงเลขจำนวนเต็มและเครื่องหมาย , เช่น 1,2,3,4,5"
button.value = "Submit"
button.type = "submit"
textArray.innerText = "Array: "
const textboxArray = document.createElement('input');
divEx1.appendChild(desc);
divEx1.appendChild(textArray);
textArray.appendChild(textboxArray);
divEx1.appendChild(p);
divEx1.appendChild(button);
const divAns = document.createElement('div');
divEx1.appendChild(divAns);
divAns.appendChild(ans);

let numberArray: number[] = [];
let ansStr: string = '';

button.addEventListener('click', () => {
  divAns.innerHTML = '';
  numberArray = [];
  let isNegative = false;
  ansStr= '';
  if (!textboxArray.value) {
    alert("Please enter a value")
  } else {
    addArrays();
    for (const element of numberArray) {
      if (element < 1 || element > 31) {
        console.log(element)
        isNegative = true;
        break;
      }
    }
    console.log(isNegative)
    if(!isNegative){
      findAnswer()
      ans.innerText = ansStr;
    }else{
      alert("Please enter positive number again")
      ans.innerText= '';
    }
  }
}
)

function addArrays() {
  let cleanData = textboxArray.value.split(" ").join("") //ใช้ split และ join แปลงข้อความจาก textbox เพื่อรอนำข้อมูลไปใส่ใน Array แล้วนำไปคำนวณต่อเท่านั้น
  if (cleanData !== '') {
    let temp: string = "";
    for (let i = 0; i < cleanData.length; i++) {
      if (cleanData[i] !== ',') {
        temp += (cleanData[i]);
        if ((i === cleanData.length - 1) && temp !== '') {
          numberArray.push(+temp)
          temp = "";
        }
        continue;
      } else {
        numberArray.push(+temp)
        temp = "";
      }
    }
    divAns.appendChild(ans);
  }
}


function findAnswer() {
  ansStr = '';
  let isLastGroup: boolean = false;
  let curNum: number = 0;
  let nextNum: number = 0;
  if (numberArray.length > 1) {
    for (let i = 0; i < numberArray.length - 1; i++) {
      curNum = numberArray[i];
      nextNum = numberArray[i + 1];
      if (i === 0 && nextNum - curNum === 1) {
        ansStr += curNum + ""
      } else if (i === 0 && nextNum - curNum !== 1) {
        ansStr += curNum + ""
        isLastGroup = true;
      } else if (nextNum - curNum === 1 && isLastGroup !== false) {
        ansStr += `, ${curNum}`
        isLastGroup = false;
      } else if (nextNum - curNum !== 1 && isLastGroup === false) {
        ansStr += ` -${curNum}`
        isLastGroup = true;
      } else if (nextNum - curNum !== 1 && isLastGroup === true) {
        ansStr += `, ${curNum}`
      }
    }
    if (nextNum - curNum === 1) {
      ansStr += ` -${nextNum}`
    } else {
      ansStr += `, ${nextNum}`
    }
  } else {
    ansStr += `${numberArray[0]}`
  }
}

export {

}