const divEx1 = document.getElementById('lab2') as HTMLDivElement;
const textArray = document.createElement('p')
const textSum = document.createElement('p')
const p = document.createElement('p');
const ans = document.createElement('p');

const button = document.createElement('input');
button.value = "Submit"
button.type = "submit"
textArray.innerText = "s1: "
textSum.innerHTML = "s2: "
const txtS1 = document.createElement('input');
const txtS2 = document.createElement('input');
divEx1.appendChild(textArray);
divEx1.appendChild(textSum);
textArray.appendChild(txtS1);
textSum.appendChild(txtS2);
divEx1.appendChild(p);
divEx1.appendChild(button);
const divAns = document.createElement('p');
divEx1.appendChild(divAns);
divAns.appendChild(ans);


button.addEventListener("click", () => {
  let tempS1: string = txtS1.value.toLowerCase();
  let tempS2: string = txtS2.value.toLowerCase();
  findTheSameChar(tempS1, tempS2)
})

function findTheSameChar(tempS1: string, tempS2: string) {
  if (txtS1.value !== '' && txtS2.value !== '') {
    for (let i = 0; i < tempS1.length; i++) {
      for (let j = 0; j < tempS2.length; j++) {
        if (tempS1.charAt(i) === tempS2.charAt(j)) {
          tempS2 = tempS2.replace(tempS2.charAt(j), '')
          break;
        }
      }
    }
    checkAns(tempS2)
  }
  else {
    alert("Please Enter a value")
    ans.innerText = "";
  }

}

function checkAns(tempS2: string) {
  if (tempS2.length === 0 && tempS2 === '' && txtS1.value.length === txtS2.value.length) {
    ans.innerText = "true";
  } else {
    ans.innerText = "false";
  }
}


export {

}