const divEx1 = document.getElementById('lab5') as HTMLDivElement;
const textArray = document.createElement('p')
const p = document.createElement('p');
const desc = document.createElement('p');
const ans = document.createElement('div');


const button = document.createElement('input');
desc.innerText = "กรอกเพียงเลขจำนวนเต็มบวก ระหว่าง 1 ถึง 4"
button.value = "Submit"
button.type = "submit"
textArray.innerText = "Input: "
const txtbox = document.createElement('input');
divEx1.appendChild(desc);
divEx1.appendChild(textArray);
textArray.appendChild(txtbox);
divEx1.appendChild(p);
divEx1.appendChild(button)
const divAns = document.createElement('div');
divEx1.appendChild(divAns);
divAns.appendChild(ans);



button.addEventListener('click', () => {
  divAns.innerHTML = '';
  let max = (+txtbox.value * 2) - 1;
  let center = customFloor(max / 2);
  let realAns: string[] = [];
  divEx1.appendChild(divAns);
  let count: number = 0;
  let fill: number = 1;
  let lastNum = false;
  if (+txtbox.value > 0 && +txtbox.value < 5) {
    for (let i = 0; i < +txtbox.value; i++) {
      let tempAns: string[] = [];
      for (let j = 0; j < max; j++) {
        if (j === center && count < i + 1) {
          tempAns.push(fill.toString())
          count++
          fill++;
          lastNum = true;
        } else {
          if (j < center) {
            tempAns.push(" ")
          } else if (count < i + 1 && lastNum == true) {
            tempAns.push(" ")
            lastNum = false;
          } else if (count < i + 1 && lastNum == false) {
            if (fill == 10) {
              fill = 0;
              tempAns.push(fill.toString())
            } else {
              tempAns.push(fill.toString())
            }
            fill += 1;
            count += 1;
            lastNum = true;
          } else {
            tempAns.push(" ")
          }
        }
      }
      count = 0;
      center -= 1;
      realAns.push(tempAns.toString());
      tempAns = [];
    }
    for (let i = 0; i < realAns.length; i++) {
      let ans = realAns[i];
      let ansArray = ans.split(''); //ใช้ split แปลงข้อความจาก ans เพื่อให้สามารถนำไปอ้างอิงตำแหน่ง index ตัวอักษรเท่านั้น 
      for (let j = 0; j < ans.length; j++) {
        if (ansArray[j] === ',') {
          ansArray[j] = '';
        }
      }
      const ansSub = document.createElement('div');
      ansSub.innerHTML = ansArray.join('&nbsp;');
      divAns.appendChild(ansSub);
    }
  } else {
    alert("Please enter number between 1 and 4")
    txtbox.value = '';
  }

}


)

function customFloor(number: number): number {
  return number >= 0 ? ~~number : ~~number - 1;
}
export {

}