const divEx1 = document.getElementById('lab7') as HTMLDivElement;
const textArray = document.createElement('p')
const p = document.createElement('p');
const desc = document.createElement('p');
const ans = document.createElement('p');


const button = document.createElement('input');
desc.innerText = "กรอกเลขจำนวนเต็มที่เป็นวินาที (second)"
button.value = "Submit"
button.type = "submit"
textArray.innerText = "Input: "
const txtbox = document.createElement('input');
divEx1.appendChild(desc);
divEx1.appendChild(textArray);
textArray.appendChild(txtbox);
divEx1.appendChild(p);
divEx1.appendChild(button)
const divAns = document.createElement('div');
divEx1.appendChild(divAns);
divAns.appendChild(ans);



button.addEventListener('click', () => {
  let input: number;
  let indexTime: number;
  divAns.innerHTML = '';
  input = + txtbox.value;
  let hours: number = 0;
  let mins: number = 0;
  let secs: number = 0;
  if (txtbox.value !== '' && isInteger(input)) {
    hours = customFloor(input / 3600)
    indexTime = customFloor(input % 3600)
    mins = customFloor(indexTime / 60)
    indexTime = customFloor(indexTime % 60)
    secs = indexTime;
    let outputTime: string = "";
    if (hours < 10) {
      outputTime += `0${hours}:`;
    } else {
      outputTime += `${hours}:`;
    }

    if (mins < 10) {
      outputTime += `0${mins}:`;
    } else {
      outputTime += `${mins}:`;
    }

    if (secs < 10) {
      outputTime += `0${secs}`;
    } else {
      outputTime += `${secs}`;
    }
    ans.innerText = outputTime;
    divAns.appendChild(ans);

  } else {
    alert("Please enter a number of seconds")
  }
}
)

function isInteger(number: number) {
  if (typeof number !== 'number' || number !== customFloor(number)) {
    return false;
  }

  return true;
}

function customFloor(number: number): number {
  return number >= 0 ? ~~number : ~~number - 1;
}

export {

}