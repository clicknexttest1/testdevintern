const divEx1 = document.getElementById('lab1') as HTMLDivElement;
const textArray = document.createElement('p')
const textSum = document.createElement('p')
const p = document.createElement('p');
const desc = document.createElement('p');
const txtErr = document.createElement('p');


const button = document.createElement('input');
desc.innerText = "กรอกเพียงเลขจำนวนเต็มและเครื่องหมาย , เช่น 1,2,3,4,5"
button.value = "Submit"
button.type = "submit"
textArray.innerText = "Array: "
textSum.innerHTML = "Sum: &nbsp&nbsp"
const textboxArray = document.createElement('input');
const textboxSum = document.createElement('input');
divEx1.appendChild(desc);
divEx1.appendChild(textArray);
divEx1.appendChild(textSum);
textArray.appendChild(textboxArray);
textSum.appendChild(textboxSum);
divEx1.appendChild(p);
divEx1.appendChild(button);
const divAns = document.createElement('div');
divEx1.appendChild(divAns);
divAns.appendChild(txtErr);

let numberArray: number[] = [];
let tempIndex: number[] = [];
button.addEventListener('click', () => {
  divAns.innerHTML = '';  //เคลียร์ช่องคำตอบเมื่อกด submit อีกครั้ง
  numberArray = []; //เคลียร์ Array เมื่อกด submit อีกครั้ง
  tempIndex = []; //เคลียร์ tempIndex เมื่อกด submit อีกครั้ง
  addArrays();
  findAnswer();
}
)

function addArrays() {
  let cleanData = textboxArray.value.split(" ").join("") //ใช้ split และ join เพื่อแปลงข้อความจาก textbox เพื่อรอนำข้อมูลไปใส่ใน Array แล้วนำไปคำนวณต่อเท่านั้น
  if (cleanData !== '' && textboxSum.value !== '') {
    let temp: string = "";
    for (let i = 0; i < cleanData.length; i++) {
      if (cleanData[i] !== ',') {
        temp += (cleanData[i]);
        if ((i === cleanData.length - 1) && temp !== '') {
          numberArray.push(+temp)
          temp = "";
        }
        continue;
      } else {
        numberArray.push(+temp)
        temp = "";
      }
    }
    divAns.appendChild(txtErr);
  } else {
    alert("Please enter a value");
  }
}

function findAnswer() {
  let sum = + textboxSum.value
  for (let i = 0; i < numberArray.length; i++) {
    for (let j = i + 1; j < numberArray.length; j++) {
      if (!tempIndex.includes(j) && !tempIndex.includes(i) && numberArray[i] + numberArray[j] === sum) {
        tempIndex.push(j)
        tempIndex.push(i)
        const asnSub = document.createElement('div');
        asnSub.innerText = `${numberArray[i]},${numberArray[j]}`
        divAns.appendChild(asnSub)
        break; //break เพราะว่าตำแหน่งของ Array นี้ ถูกใช้ไปแล้วจะไม่สามารถนำมา บวก เปรียบเทียบกันได้อีก
      }
    }
  }
}

export {

}